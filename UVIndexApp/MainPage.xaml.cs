﻿using BackgroundTask;
using EPAUVIndex;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml.Linq;
using Windows.Devices.Geolocation;
using Windows.Media.SpeechRecognition;
using Windows.Networking.Connectivity;
using Windows.Phone.UI.Input;
using Windows.Storage;
using Windows.UI;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;

namespace UVIndexApp
{
    public sealed partial class MainPage : Page
    {
        #region variables
        const string googleMapsBase = "http://maps.googleapis.com/maps/api/geocode/xml?address=";
        const string googleMapsAppend = "&sensor=true";
        ApplicationDataContainer store = Windows.Storage.ApplicationData.Current.RoamingSettings;
        ApplicationDataContainer localStore = Windows.Storage.ApplicationData.Current.LocalSettings;
        static string TaskName = "UV level updater";
        Grid zipPrompt;
        #endregion

        //running the program
        public MainPage()
        {
            this.InitializeComponent();
            this.NavigationCacheMode = NavigationCacheMode.Required;
            System.Diagnostics.Debug.WriteLine("MainPage");
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            runProg();
        }
        async private void runProg()
        {
            //runs the program
            bool allowLoc;
            sizeCircle(-1, false); //initially create circle without animations
            if (isFirstLaunch())
            {
                allowLoc = await askForLocation();
            }
            else
            {
                allowLoc = (bool)store.Values["allowLoc"];
            }
            registerBackgroundTask();
            updateVoiceCommands();
            hideStatusBar();
            refresh(allowLoc, false, true);
        }

        //launch helpers
        private void hideStatusBar()
        {
            //hides the status bar for app
            StatusBar s = Windows.UI.ViewManagement.StatusBar.GetForCurrentView();
            s.HideAsync();
        }
        private bool isFirstLaunch()
        {
            //checks whether this is the first time a user has ever lanched the app, and then 
            //counts the number of times it has been launched
            if (!store.Values.ContainsKey("numTimesRun"))
            {
                store.Values["numTimesRun"] = 1;
                return true;
            }
            store.Values["numTimesRun"] = (int)store.Values["numTimesRun"] + 1;
            return false;
        }
        async private Task<bool> askForLocation()
        {
            //creates a dialog that asks wheter autolocate can be used
            MessageDialog d = new MessageDialog("Would you like Sunny to use your location?");
            d.Commands.Add(new UICommand("Yes"));
            d.Commands.Add(new UICommand("No"));
            var result = await d.ShowAsync();
            if (result.Label == "Yes")
            {
                store.Values["allowLoc"] = true;
                return true;
            }
            store.Values["allowLoc"] = false;
            return false;
        }
        private void registerBackgroundTask()
        {
            //Runs a single instance of the backgrouond task to get the most up to date
            //tile after the app is launched
            UpdateTiles.RunFromApp();
            //Registers app to run for the first time on a new phone
            if (!localStore.Values.ContainsKey("bgRegistered") || !rateIsUnchanged())
            {
                localStore.Values["bgRegistered"] = true;
                if (store.Values.ContainsKey("allowBackground"))
                {
                    if ((bool)store.Values["allowBackground"])
                    {
                        if (store.Values.ContainsKey("updateFreq"))
                        {
                            var rate = Convert.ToUInt32(store.Values["updateFreq"]);
                            UpdateTiles.Register(rate);
                        }
                        else
                        {
                            store.Values["updateFreq"] = 120;
                            var rate = Convert.ToUInt32(120);
                            UpdateTiles.Register(rate);
                        }
                    }
                    else
                    {
                        UpdateTiles.Unregister(TaskName);
                    }
                }
                else
                {
                    store.Values["allowBackground"] = true;
                    UpdateTiles.Register(Convert.ToUInt32(120));
                }
            }
        }
        async private void updateVoiceCommands()
        {
            //refreshes the voice commands
            VoiceCommandManager.InstallCommandSetsFromStorageFileAsync(await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///UVVoiceCommands.xml")));
        }


        //actually updating info
        async private void refresh(bool allowLoc, bool changeLoc, bool showRefresh)
        {
            //refreshes the data, including a progress bar
            if (!changeLoc)
            {
                clearElements();
            }
            ProgressBar progBar = new ProgressBar() { Width = 250, HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Center, Foreground = new SolidColorBrush(Colors.White) };
            Grid progBarGrid = new Grid() { Background = new SolidColorBrush(new Color() { A = 0, B = 0, G = 0, R = 0 }) };
            TextBlock text = new TextBlock() { Text = "updating...", HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Center, VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Center, Margin = new Thickness(0, 30, 0, 0), FontSize = 18 };
            if (showRefresh)
            {
                clearElements();
                sizeCircle(-1, true);
            }
            progBarGrid.Children.Add(progBar);
            progBarGrid.Children.Add(text);
            progBar.IsIndeterminate = true;
            main.Children.Add(progBarGrid);
            await updateInfo(allowLoc, changeLoc);
            progBar.IsIndeterminate = false;
            main.Children.Remove(progBarGrid);
        }
        async private Task updateInfo(bool allowLoc, bool changeLoc)
        {
            //gets the info needed for app
            if (isInternet())
            {
                if (allowLoc)
                {
                    string zip = await getZip(await getGeo());
                    if (zip != null)
                    {
                        PullIndex indGet = new PullIndex(zip);
                        DayForecast day = await indGet.getDayUV();
                        placeData(day.UV_INDEX, zip);
                    }
                    else
                    {
                        placeData(-1, "-----");
                    }
                }
                else
                {
                    if (zipSaved() && !changeLoc)
                    {
                        PullIndex indGet = new PullIndex(store.Values["savedZip"].ToString());
                        DayForecast day = await indGet.getDayUV();
                        placeData(day.UV_INDEX, store.Values["savedZip"].ToString());
                    }
                    else
                    {
                        promptForZip();
                    }
                }
            }
            else
            {
                placeData(-1, "-----");
            }
        }
        public static bool isInternet()
        {
            //checks connections to make sure that they are availible
            ConnectionProfile connections = NetworkInformation.GetInternetConnectionProfile();
            bool internet = connections != null && connections.GetNetworkConnectivityLevel() == NetworkConnectivityLevel.InternetAccess;
            return internet;
        }
        private bool rateIsUnchanged()
        {
            //checks whether the update rate is unchanged on other devices,
            //so the rate can then be synced accross devices
            if (store.Values.ContainsKey("updateFreq"))
            {
                if (localStore.Values.ContainsKey("updateFreq"))
                {
                    uint roamVal = Convert.ToUInt32(store.Values["updateFreq"]);
                    uint locVal = Convert.ToUInt32(localStore.Values["updateFreq"]);
                    bool isUnchanged = roamVal == locVal;
                    if (isUnchanged)
                    {
                        return isUnchanged;
                    }
                    localStore.Values["updateFreq"] = store.Values["updateFreq"];
                    return isUnchanged;
                }
                localStore.Values["updateFreq"] = store.Values["updateFreq"];
                return false;
            }
            return false;
        }
        private void clearElements()
        {
            //clears everything off the screen for loading
            ss.Text = "";
            zipCode.Text = "-----";
            uvText.Text = "";
            label.Text = "";
        }
        private bool zipSaved()
        {
            //checks whether there is a saved zip code
            return store.Values.ContainsKey("savedZip");
        }

        //get custom zip code
        private void promptForZip()
        {
            //creates a grid that promts for a zip code
            zipPrompt = new Grid() { Background = new SolidColorBrush() { Color = new Color() { A = 175, R = 0, G = 0, B = 0 } } };
            TextBox t = new TextBox();
            InputScopeName n = new InputScopeName();
            InputScope scope = new InputScope();
            n.NameValue = InputScopeNameValue.Number;
            scope.Names.Add(n);
            t.AcceptsReturn = false;
            t.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Stretch;
            t.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Top;
            t.PlaceholderText = "Enter a 5 digit US Zip Code";
            t.InputScope = scope;
            t.TextChanged += t_TextChanged;
            zipPrompt.Children.Add(t);
            zipPrompt.Tapped += zipGrid_Tapped;
            main.Children.Add(zipPrompt);
            HardwareButtons.BackPressed += HardwareButtons_BackPressed;
            t.Focus(FocusState.Programmatic);
        }
        void zipGrid_Tapped(object sender, TappedRoutedEventArgs e)
        {
            //removes zip prompt on tap
            main.Children.Remove((sender as Grid));
        }
        void t_TextChanged(object sender, TextChangedEventArgs e)
        {
            //gets whatever is typed, and uses it on the 5th number pressed
            string input = (sender as TextBox).Text;
            if (input.Length == 5)
            {
                store.Values["savedZip"] = input;
                updateInfo(false, false);
                if (main.Children.Contains(zipPrompt))
                {
                    main.Children.Remove(zipPrompt);
                    zipPrompt = null;
                }
            }
        }

        //gets geoposition and converts it to zip code
        async private Task<string> getGeo()
        {
            //gets the geoloc
            try
            {
                Geolocator geo = new Geolocator();
                Geopoint point = (await geo.GetGeopositionAsync(new TimeSpan(2, 0, 0), new TimeSpan(0, 0, 0, 2))).Coordinate.Point;
                return point.Position.Latitude + "," + point.Position.Longitude;
            }
            catch
            {
                return "";
            }
        }
        async private Task<string> getZip(string p)
        {
            //converts a set of coordinates to a zip code
            if (p != "")
            {
                Uri uri = new Uri(googleMapsBase + p + googleMapsAppend);
                HttpClient client = new HttpClient();
                XDocument doc = XDocument.Load(await client.GetStreamAsync(uri));

                if (doc.Element("GeocodeResponse").Element("status").Value != "ZERO_RESULTS")
                {
                    IEnumerable<XElement> zip = doc.Element("GeocodeResponse").Element("result").Elements("address_component");
                    foreach (XElement e in zip)
                    {
                        if (e.Element("type").Value == "postal_code")
                        {
                            return e.Element("long_name").Value;
                        }
                    }
                }
                return null;
            }
            return null;
        }

        //updates app data
        private void placeData(int uvIndex, string zip)
        {
            //a central place to get all the different parts of data onscreen
            sizeCircle(uvIndex, true);
            addUVIndex(uvIndex);
            addZip(zip);
            addSunScreen(uvIndex);
            addLabel(uvIndex);
        }
        private void addLabel(int uvIndex)
        {
            //adds a label if there are no errors
            if (uvIndex != -1)
            {
                label.Visibility = Windows.UI.Xaml.Visibility.Visible;
                label.Text = "UV";
            }
            else
            {
                label.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            }
        }
        private void addZip(string zip)
        {
            //displays the zip code
            zipCode.Text = zip;
        }
        private void addSunScreen(int uvIndex)
        {
            //adds the label on whether you need sunscreen
            ss.Text = needSunScreen(uvIndex);
            ss.Margin = new Thickness(0, getDiameter(uvIndex) + 60, 0, 0);
        }
        private void sizeCircle(int uV, bool animate)
        {
            //sets the circle size
            if (animate)
            {
                circleSizeAnimation(uV);
                circleColorAnimation(uV);
            }
            else
            {
                circle.Fill = new SolidColorBrush(getCircleColor(uV));
                circle.Height = circle.Width = getDiameter(uV);
            }
        }
        private void circleColorAnimation(int uV)
        {
            //animates the changing colors of circle
            Color c = (circle.Fill as SolidColorBrush).Color;
            ColorAnimation a = new ColorAnimation()
            {
                From = c,
                To = getCircleColor(uV),
                Duration = TimeSpan.FromMilliseconds(500),
                EnableDependentAnimation = true
            };
            Storyboard.SetTargetProperty(a, "(Ellipse.Fill).(SolidColorBrush.Color)");
            Storyboard.SetTarget(a, circle);
            Storyboard s = new Storyboard();
            s.Children.Add(a);
            s.Begin();
        }
        private void circleSizeAnimation(int uvIndex)
        {
            //animates resizing of circle with bouncing
            ElasticEase ease = new ElasticEase() { EasingMode = EasingMode.EaseOut, Oscillations = 2, Springiness = .5 };
            TimeSpan duration = TimeSpan.FromMilliseconds(1000);
            double newDiameter = getDiameter(uvIndex);
            double oldDiameter = circle.Height;
            DoubleAnimation widthAnimation = new DoubleAnimation()
            {
                EasingFunction = ease,
                From = oldDiameter,
                To = newDiameter,
                Duration = duration,
                EnableDependentAnimation = true
            };
            DoubleAnimation heightAnimation = new DoubleAnimation()
            {
                EasingFunction = ease,
                From = oldDiameter,
                To = newDiameter,
                Duration = duration,
                EnableDependentAnimation = true
            };
            Storyboard.SetTargetProperty(widthAnimation, "Width");
            Storyboard.SetTarget(widthAnimation, circle);
            Storyboard.SetTargetProperty(heightAnimation, "Height");
            Storyboard.SetTarget(heightAnimation, circle);
            Storyboard s = new Storyboard();
            s.Children.Add(widthAnimation);
            s.Children.Add(heightAnimation);
            s.Begin();
        }
        private void addUVIndex(int uvIndex)
        {
            //adds the uv index in the center of the circle, if theres no errors
            if (uvIndex == -1)
            {
                uvText.Text = "Error";
                uvText.FontSize = 40;
            }
            else
            {
                uvText.Text = uvIndex.ToString();
                uvText.FontSize = 50;
            }
            uvText.Foreground = new SolidColorBrush(Colors.White);
        }
        private int getDiameter(int uvIndex)
        {
            //sets the diameter of the circle
            if (uvIndex == -1)
            {
                return 250;
            }
            return (uvIndex + 10) * 15;
        }
        private string needSunScreen(int uV)
        {
            //displays a string with either an error message, or the reccomended protection
            if (uV == -1)
            {
                return "There was a problem getting data, make sure you are in the United States, or entered a valid zip code. If the problem continues, check your network connection";
            }
            else if (uV < 3)
            {
                return "You shouldn't need any sun protection today";
            }
            else if (uV < 6)
            {
                return "You might want at least SPF 15 sunscreen today";
            }
            else if (uV < 8)
            {
                return "You should wear at least SPF 15 sunscreen today";
            }
            else if (uV < 11)
            {
                return "You might want at least SPF 30 sunscreen today";
            }
            else
            {
                return "You need at least SPF 30 sunscreen, and should probably avoid excessive sun today";
            }
        }
        private Color getCircleColor(int uV)
        {
            //chooses a color for the circle
            Color c = new Color();
            c.A = 255;
            switch (uV)
            {
                case -1:
                    c.R = 127;
                    c.G = 127;
                    c.B = 127;
                    break;
                case 0:
                case 1:
                case 2:
                    c.R = 255;
                    c.G = 180;
                    c.B = 71;
                    break;
                case 3:
                case 4:
                case 5:
                    c.R = 255;
                    c.G = 140;
                    c.B = 71;
                    break;
                case 6:
                case 7:
                case 8:
                    c.R = 255;
                    c.G = 100;
                    c.B = 71;
                    break;
                case 9:
                case 10:
                case 11:
                    c.R = 255;
                    c.G = 80;
                    c.B = 71;
                    break;
                default:
                    c.R = 255;
                    c.G = 60;
                    c.B = 71;
                    break;
            }
            return c;
        }


        //appbar buttons
        private void refreshButton_Click(object sender, RoutedEventArgs e)
        {
            //refreshes the app without changing location
            registerBackgroundTask();
            refresh((bool)store.Values["allowLoc"], false, true);
        }
        private void changeLoc_Click(object sender, RoutedEventArgs e)
        {
            //refreshes the app, asking for a zip code
            store.Values["allowLoc"] = false;
            registerBackgroundTask();
            refresh(false, true, false);
        }
        private void currentLoc_Click(object sender, RoutedEventArgs e)
        {
            //refreshes the app, using geolocation for location
            store.Values["allowLoc"] = true;
            registerBackgroundTask();
            refresh(true, false, true);
        }
        private void about_Click(object sender, RoutedEventArgs e)
        {
            //opens the about page
            Frame.Navigate(typeof(AboutPage));
        }
        private void settings_Click(object sender, RoutedEventArgs e)
        {
            //opens the settings page
            Frame.Navigate(typeof(SettingsPage));
        }

        //back button override
        private void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            //override back button to remove zip prompt
            if (main.Children.Contains(zipPrompt))
            {
                e.Handled = true;
                main.Children.Remove(zipPrompt);
                zipPrompt = null;
            }
        }
    }
}
