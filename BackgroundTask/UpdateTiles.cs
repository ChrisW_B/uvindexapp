﻿using EPAUVIndex;
using NotificationsExtensions.TileContent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using System.Xml.Linq;
using Windows.ApplicationModel.Background;
using Windows.Devices.Geolocation;
using Windows.Graphics.Display;
using Windows.Graphics.Imaging;
using Windows.Storage;
using Windows.UI;
using Windows.UI.Notifications;
using Windows.UI.Text;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Shapes;

namespace BackgroundTask
{
    public sealed class UpdateTiles : XamlRenderingBackgroundTask
    {
        const string googleMapsBase = "http://maps.googleapis.com/maps/api/geocode/xml?address=";
        const string googleMapsAppend = "&sensor=true";
        static string TaskName = "UV level updater";
        ApplicationDataContainer store = ApplicationData.Current.RoamingSettings;

        enum tileSize
        {
            wide,
            medium
        }

        //register or unregister the task
        public async static void Register(uint mins)
        {
            if (IsTaskRegistered(TaskName))
            {
                Unregister(TaskName);
            }
            BackgroundAccessStatus result = await BackgroundExecutionManager.RequestAccessAsync();
            BackgroundTaskBuilder builder = new BackgroundTaskBuilder();
            builder.Name = TaskName;
            builder.TaskEntryPoint = typeof(UpdateTiles).FullName;
            builder.SetTrigger(new TimeTrigger(mins, false));
            SystemCondition condition = new SystemCondition(SystemConditionType.InternetAvailable);
            builder.AddCondition(condition);
            builder.Register();
        }
        public async static void RunFromApp()
        {
            //allows the background task to auto run once
            if (IsTaskRegistered(TaskName + "Mini"))
            {
                Unregister(TaskName + "Mini");
            }
            BackgroundAccessStatus result = await BackgroundExecutionManager.RequestAccessAsync();
            BackgroundTaskBuilder builder = new BackgroundTaskBuilder();
            builder.Name = TaskName + "Mini";
            builder.TaskEntryPoint = typeof(UpdateTiles).FullName;
            builder.SetTrigger(new TimeTrigger(15, true));
            SystemCondition condition = new SystemCondition(SystemConditionType.InternetAvailable);
            builder.AddCondition(condition);
            builder.Register();
        }
        public static void Unregister(string name)
        {
            var entry = BackgroundTaskRegistration.AllTasks.FirstOrDefault(kvp => kvp.Value.Name == name);
            if (entry.Value != null)
            {
                entry.Value.Unregister(true);
            }
        }
        public static bool IsTaskRegistered(string name)
        {
            var entry = BackgroundTaskRegistration.AllTasks.FirstOrDefault(kvp => kvp.Value.Name == name);
            if (entry.Value != null)
            {
                return true;
            }
            return false;
        }

        //runs the task
        async protected override void OnRun(IBackgroundTaskInstance tI)
        {
            BackgroundTaskDeferral def = tI.GetDeferral();
            await updateTile();
            def.Complete();
        }

        async private Task updateTile()
        {
            string zip;
            if (allowLoc())
            {
                zip = await getZip(await getGeo());

            }
            else if (savedZip())
            {
                zip = (string)store.Values["savedZip"];
            }
            else
            {
                return;
            }
            PullIndex indGet = new PullIndex(zip);
            if (zip == null)
            {
                await createTileImages(createFailGrid(tileSize.medium), createFailGrid(tileSize.wide), -1);
            }
            else
            {
                DayForecast day = await indGet.getDayUV();
                await createTileImages(createCircleGrid(day.UV_INDEX, tileSize.medium), createCircleGrid(day.UV_INDEX, tileSize.wide), day.UV_INDEX);
            }

        }

        private bool savedZip()
        {
            return store.Values.ContainsKey("savedZip");
        }

        private bool allowLoc()
        {
            if (store.Values.ContainsKey("allowLoc"))
            {
                return (bool)store.Values["allowLoc"];
            }
            return false;
        }

        //making the tile image
        async private Task createTileImages(UIElement mediumCircle, UIElement wideCircle, int uV)
        {
            //creates a file to hold the image to place on tiles
            string locFolderLoc = "ms-appdata:///local/";
            string mediumTileName = "medTileImage.png";
            string wideTileName = "wideTileImage.png";
            await renderTile(mediumCircle, mediumTileName);
            await renderTile(wideCircle, wideTileName);
            pushImageToTiles(locFolderLoc + mediumTileName, locFolderLoc + wideTileName, uV);
        }
        async private Task renderTile(UIElement circle, string tileName)
        {
            RenderTargetBitmap bm = new RenderTargetBitmap();

            await bm.RenderAsync(circle);
            Windows.Storage.Streams.IBuffer pixBuf = await bm.GetPixelsAsync();

            StorageFolder localFolder = ApplicationData.Current.LocalFolder;
            StorageFile tileImageFile = await localFolder.CreateFileAsync(tileName, CreationCollisionOption.ReplaceExisting);
            DisplayInformation dispInfo = DisplayInformation.GetForCurrentView();

            using (var stream = await tileImageFile.OpenAsync(FileAccessMode.ReadWrite))
            {
                var encoder = await BitmapEncoder.CreateAsync(BitmapEncoder.PngEncoderId, stream);
                encoder.SetPixelData(BitmapPixelFormat.Bgra8, BitmapAlphaMode.Straight, (uint)bm.PixelWidth, (uint)bm.PixelHeight, dispInfo.LogicalDpi, dispInfo.LogicalDpi, pixBuf.ToArray());
                await encoder.FlushAsync();
            }
        }
        private void pushImageToTiles(string mediumTileLoc, string wideTileLoc, int uV)
        {
            //pushes the image to the tiles
            string tileString = needSunScreen(uV);
            ITileSquare150x150PeekImageAndText04 mediumTile = TileContentFactory.CreateTileSquare150x150PeekImageAndText04();
            mediumTile.TextBodyWrap.Text = tileString;
            mediumTile.Branding = TileBranding.None;
            mediumTile.Image.Alt = "altMed";
            mediumTile.Image.Src = mediumTileLoc;
            mediumTile.StrictValidation = true;

            ITileWide310x150PeekImage03 wideTile = TileContentFactory.CreateTileWide310x150PeekImage03();
            wideTile.TextHeadingWrap.Text = tileString;
            wideTile.Branding = TileBranding.None;
            wideTile.Image.Alt = "altWide";
            wideTile.Image.Src = wideTileLoc;
            wideTile.Square150x150Content = mediumTile;
            wideTile.StrictValidation = true;

            TileNotification wideNotif = wideTile.CreateNotification();
            TileUpdateManager.CreateTileUpdaterForApplication().Clear();
            TileUpdateManager.CreateTileUpdaterForApplication().Update(wideNotif);
        }


        //creating ui elements
        private UIElement createCircleGrid(int uv, tileSize size)
        {
            //creates grid that holds the two elements
            Grid g = createGrid();
            StackPanel s = createStackpanel();
            Ellipse c = createCircle(uv);
            TextBlock t = addUVIndex(uv);
            TextBlock l = addLabel(uv);
            g.Children.Add(c);
            s.Children.Add(l);
            s.Children.Add(t);
            g.Children.Add(s);
            if (size == tileSize.medium)
            {
                g.Width = 150;
            }
            return g;
        }
        private UIElement createFailGrid(tileSize size)
        {
            Grid g = createGrid();
            Ellipse c = createCircle(-1);
            TextBlock t = addUVIndex(-1);
            g.Children.Add(c);
            g.Children.Add(t);
            if (size == tileSize.medium)
            {
                g.Width = 150;
            }
            return g;
        }
        private Grid createGrid()
        {
            Grid g = new Grid();
            g.Background = new SolidColorBrush(Colors.Transparent);
            g.Width = 310;
            g.Height = 150;
            g.UseLayoutRounding = true;
            return g;
        }
        private StackPanel createStackpanel()
        {
            StackPanel s = new StackPanel();
            s.Orientation = Orientation.Horizontal;
            s.HorizontalAlignment = HorizontalAlignment.Center;
            s.VerticalAlignment = VerticalAlignment.Center;
            return s;
        }
        private TextBlock addLabel(int uv)
        {
            TextBlock t = new TextBlock();
            if (uv != -1)
            {
                t.Text = "UV";
            }
            t.FontFamily = new FontFamily("Segoe WP Black");
            t.FontWeight = new FontWeight() { Weight = 900 };
            t.FontSize = 11;
            t.HorizontalAlignment = HorizontalAlignment.Center;
            t.Margin = new Thickness(0, 5, 0, 0);
            return t;
        }
        private Ellipse createCircle(int uvIndex)
        {
            //creates a circle indicator
            Ellipse e = new Ellipse();
            e.Fill = new SolidColorBrush(getCircleColor(uvIndex));
            e.Width = e.Height = getRadius(uvIndex);
            if (uvIndex == -1)
            {
                e.Width = e.Height = 100;
            }
            return e;
        }
        private TextBlock addUVIndex(int uvIndex)
        {
            //creates a textblock holding the UV Index
            TextBlock t = new TextBlock();
            t.TextAlignment = TextAlignment.Center;
            t.HorizontalAlignment = HorizontalAlignment.Center;
            if (uvIndex == -1)
            {
                t.Text = "Error";
                t.FontSize = 20;
                t.VerticalAlignment = VerticalAlignment.Center;
            }
            else
            {
                t.Text = uvIndex.ToString();
                t.FontSize = 30;
            }
            t.Foreground = new SolidColorBrush(Colors.White);
            return t;
        }
        private int getRadius(int uvIndex)
        {
            //calculates a radius
            return (uvIndex * 5) + 60;
        }
        private Color getCircleColor(int p)
        {
            //gets a color for the circle
            Color c = new Color();
            c.A = 255;
            switch (p)
            {
                case -1:
                    c.R = 127;
                    c.G = 127;
                    c.B = 127;
                    break;
                case 0:
                case 1:
                case 2:
                    c.R = 255;
                    c.G = 180;
                    c.B = 71;
                    break;
                case 3:
                case 4:
                case 5:
                    c.R = 255;
                    c.G = 140;
                    c.B = 71;
                    break;
                case 6:
                case 7:
                case 8:
                    c.R = 255;
                    c.G = 100;
                    c.B = 71;
                    break;
                case 9:
                case 10:
                case 11:
                    c.R = 255;
                    c.G = 80;
                    c.B = 71;
                    break;
                default:
                    c.R = 255;
                    c.G = 60;
                    c.B = 71;
                    break;
            }
            return c;
        }
        private string needSunScreen(int uV)
        {
            if (uV == -1)
            {
                return "Make sure your zip code is valid, and check the app for more info";
            }
            else if (uV < 3)
            {
                return "You shouldn't need any sun protection today";
            }
            else if (uV < 6)
            {
                return "You might want at least SPF 15 sunscreen today";
            }
            else if (uV < 8)
            {
                return "You should wear at least SPF 15 sunscreen today";
            }
            else if (uV < 11)
            {
                return "You might want at least SPF 30 sunscreen today";
            }
            else
            {
                return "You need at least SPF 30 sunscreen, and should probably avoid excessive sun today";
            }
        }


        //location info
        async private Task<string> getGeo()
        {
            //gets the geoloc
            Geolocator geo = new Geolocator();
            Geopoint point = (await geo.GetGeopositionAsync(new TimeSpan(2, 0, 0), new TimeSpan(0, 0, 0, 2))).Coordinate.Point;

            return point.Position.Latitude + "," + point.Position.Longitude;
        }
        async private Task<string> getZip(string p)
        {
            //converts a set of coordinates to a zip code
            Uri uri = new Uri(googleMapsBase + p + googleMapsAppend);
            HttpClient client = new HttpClient();
            XDocument doc = XDocument.Load(await client.GetStreamAsync(uri));

            if (doc.Element("GeocodeResponse").Element("status").Value != "ZERO_RESULTS")
            {
                IEnumerable<XElement> zip = doc.Element("GeocodeResponse").Element("result").Elements("address_component");
                foreach (XElement e in zip)
                {
                    if (e.Element("type").Value == "postal_code")
                    {
                        return e.Element("long_name").Value;
                    }
                }
            }
            return null;
        }
    }
}
