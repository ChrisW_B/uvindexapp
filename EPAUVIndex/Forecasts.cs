﻿using System.Collections.ObjectModel;
namespace EPAUVIndex
{
    public class Forecasts
    {
        public HourlyForecast hourly { get; set; }
        public DayForecast day { get; set; }
    }
    public class HourlyForecast
    {
        public ObservableCollection<Hour> hours = new ObservableCollection<Hour>();
    }
    public class Hour
    {
        public int SEQUENCE { get; set; }
        public string CITY { get; set; }
        public string STATE { get; set; }
        public string DATE_TIME { get; set; }
        public int UV_VALUE { get; set; }
    }

    public class DayForecast
    {
        public int ZIP_CODE { get; set; }
        public int UV_INDEX { get; set; }
        public int UV_ALERT { get; set; }
    }
}
