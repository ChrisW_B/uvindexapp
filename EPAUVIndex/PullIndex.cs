﻿using Newtonsoft.Json;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace EPAUVIndex
{
    public class PullIndex
    {
        private string urlTail;
        private const string APIBASE = "http://iaspub.epa.gov/enviro/efservice/getEnvirofactsUV";
        private const string HOURLYBASE = "HOURLY";
        private const string DAILYBASE = "DAILY";

        public PullIndex(string zipCode)
        {
            urlTail = "/ZIP/" + zipCode;
        }
        public PullIndex(int zipCode)
        {
            urlTail = "/ZIP/" + zipCode;
        }
        public PullIndex(string city, string stateAbbrev)
        {
            urlTail = "/CITY/" + city + "/STATE/" + stateAbbrev;
        }

        async public Task<Forecasts> getUVData()
        {
            HourlyForecast hours = await getHourlyData(new Uri(APIBASE + HOURLYBASE + urlTail));
            DayForecast day = await getDayData(new Uri(APIBASE + DAILYBASE + urlTail));

            return new Forecasts() { day = day, hourly = hours };
        }
        async public Task<HourlyForecast> getHourlyUV()
        {
            return await getHourlyData(new Uri(APIBASE + HOURLYBASE + urlTail));
        }
        async public Task<DayForecast> getDayUV()
        {
            return await getDayData(new Uri(APIBASE + DAILYBASE + urlTail));
        }

        async private Task<HourlyForecast> getHourlyData(Uri uri)
        {
            try
            {
                HttpClient client = new HttpClient();
                Stream str = await client.GetStreamAsync(uri);

                return toHourly(XDocument.Load(str));
            }
            catch
            {
                HourlyForecast h = new HourlyForecast() { hours = new ObservableCollection<Hour>() };
                h.hours.Add(new Hour() { UV_VALUE = -1 });
                return h;
            }
        }

        async private Task<DayForecast> getDayData(Uri uri)
        {
            try
            {
                HttpClient client = new HttpClient();
                Stream str = await client.GetStreamAsync(uri);
                return toDay(XDocument.Load(str));
            }
            catch
            {
                return new DayForecast() { UV_INDEX = -1 };
            }
        }

        private DayForecast toDay(XDocument doc)
        {
            DayForecast day = new DayForecast();
            XElement e = doc.Element("getEnvirofactsUVDAILYList").Element("getEnvirofactsUVDAILY").Element("UV_INDEX");
            if (e != null)
            {
                day.UV_INDEX = Convert.ToInt16(e.Value);
            }
            else
            {
                day.UV_INDEX = -1;
            }
            return day;
        }

        private HourlyForecast toHourly(XDocument doc)
        {
            HourlyForecast hours = new HourlyForecast();
            hours.hours = new ObservableCollection<Hour>();
            XElement e = doc.Element("getEnvirofactsUVHOURLYList");
            if (e != null)
            {
                foreach (XElement hour in e.Elements("getEnvirofactsUVHOURLY"))
                {
                    hours.hours.Add(new Hour() { SEQUENCE = Convert.ToInt16(hour.Element("ORDER").Value), UV_VALUE = Convert.ToInt16(hour.Element("UV_VALUE").Value) });
                }
            }
            return hours;
        }
    }
}
